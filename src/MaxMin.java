import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Administrator on 2016/2/23/0023.
 */
public class MaxMin {
    public static int SIZE  = 10;

    public static void main(String[] args) {
        Integer max = null;
        Integer min = null;
        boolean isNull = true;

        //Generating an Arraylist of random numbers.
        Random rand = new Random();
        ArrayList<Integer> number_list = new ArrayList<>();
        for (int i = 0; i < SIZE; i++ ) {
            number_list.add(rand.nextInt(1000));
        }

        //Finding the min and max number at the same time.
        for (Integer m : number_list ) {
            if (isNull) {
                max = m;
                min = m;
                isNull = false;
            } else {
                if (m > max) {
                    max = m;
                } else if (m < min) {
                    min = m;
                }
            }
           // System.out.println(m);
        }

        //Printing out the result.
        System.out.println("The minimum number is " + min);
        System.out.println("The maximum number is " + max);

    }
}
